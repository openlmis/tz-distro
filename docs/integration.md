# Integrations

The following integration document lays out each integration as a general use-case
centered around a visual component / deployment diagram, noting component responsibilities
for the given use-case, and protocols + format of Messages.

---
**WARNING**
many pieces are still be created as noted by looking for `?`, `WIP` and `todo` elements.

---

---
**NOTE**
ideally this page would also note error handling and/or message patterns, as many of the
use-cases appear brittle.  i.e. the non-happy-path shouldn't be on the diagram, however
we should note what the general message pattern is for those non-happy paths.

---

**NOTE**
a key difference in some use-cases is that Zanzibar (ZAN) has no middle layer operated by an
external org, whereas in Tanzania mainland there are 2:  `TanzaniaHIM` and `Muungano Gateway`

---

[[_TOC_]]

## A word on alignment

As of this writing the implemented eHealth Architecture poses a number of challenges for any
system to align with others in the following ways:

1. Multiple mainland interoperability layers on mainland confuses if integration is to one or both.
1. No facility registry that covers Zanzibar leaves seperate master data quality at multiple endpoints.
1. No National Product Catalog leaves master data quality to suffer at multiple endpoints.

Recommendations on the above:

1. Put policy in place that any one system would ever integrate into one interoperability layer,
   and where there are use-cases between endpoints across them, enforce that the overall route
   is between TanzaniaHIM and Muungano Gateway.  This keeps end systems de-coupled from the organizational
   interoperability layer boundries.
2. Focus around using mCSD, so that as new endpoints are added, any future Master Facility List for
   Zanzibar will either be managed in the HFR, or the federated nature of mCSD will allow the Zanzibar
   HFR to be sourced through the Tanzania HFR.  This keeps end systems decoupled from multiple registries
   in use.
3. Implement a National Product Catalog, this will aid in data quality, especially for use-cases that span
   flows from HMIS, LMIS, and warehousing.

## Proof of Delivery (TZ) (WIP)

```plantuml
@startuml
skinparam componentStyle rectangle

frame "OpenLMIS" {
    component fulfillment [
        Fulfillment Service
        
        Holds an empty draft
        proof of delivery doc
        from a shipment. That
        is filled in from 
        client.
    ]
}

frame "TananiaHIM" {
    component someMediator [
        Some Mediator?

        Forwards/translates proof 
        of delivery from MSD.
    ]

    [someMediator] -> [fulfillment]: Proof of Delivery over HTTP(s)
}
@enduml
```

- Protocol: HTTP(s)
- Format: Fulfillment Service's Proof of Delivery (todo: link)
- Given: Shipment has been created in Fulfillment Service (todo: link)
- When: Proof of delivery occurs from MSD and notifies to TanzaniaHIM.
- Then: Some Mediator (todo: link) retrieves/references empty draft Proof
  of Delivery from Fulfillment service, and fills it in.

## MSD Order Fulfillment (TZ)

```plantuml
@startuml
skinparam componentStyle rectangle

frame "OpenLMIS" {
    component fulfillment [
        Fulfillment Service

        Create an Order message configured
        for MSD Epicor, placed in the filesystem
        and then sFTPd to target system for 
        consumption by Epicor.
    ]
}

frame "MSD" {
    component epicor [
        Epicor ERP

        Polls filesystem/sFTP
        for incoming Order message files, and 
        processing them into shipment(s).
    ]
}

[fulfillment] -> [epicor] : Order message over sFTP
@enduml
```

- Protocol: Channel is sFTP w/ files, however no protocol in use for receipt/validation.
- Format: Order message (csv) (todo: link)
- Given: Order has been created in OpenLMIS Fulfillment service (todo: link)
- When: Order has been approved in OpenLMIS Fulfillment service (todo: link)
- Then: Fulfillment Service sFTP's to configured Supply Line's server, creating
  Order message file and placing it in designated directory with the filename
  set to the configured template.

## CMS Order Placement (ZAN) (WIP)

TODO

- Protocol: Channel is sFTP w/ files, however no protocol in use for receipt/validation.
- Format: Order message (csv) (todo: link)
- Given: 
- When:
- Then:

## CMS Order Fulfillment (ZAN) (wIP)

TODO

- Protocol:
- Format:
- Given:
- When:
- Then:

## Stock on Hand (TZ) (WIP)

```plantuml
@startuml
skinparam componentStyle rectangle

frame OpenLMIS {
    component nifi [
        Nifi Service

        Collect and aggregate stock metrics
        and indicators for each product,
        generating a MeassureReport with that
        data and Meassure with a definition for
        that data.
    ]

    component fhirServer [
        FHIR Server

        Store and search for FHIR resources.
    ]
}

frame TanzaniaHIM {
    component someMediator [
        Some Mediator

        Subscribed to new FHIR MeassureReports
        in the FHIR Server, taking those and
        transforming them to ? for Epicor
    ]
}

frame MSD {
    component epicor [
        Epicor ERP

        Receives Stock on Hand messages
    ]
}

[nifi] -> [fhirServer] : Meassure
[nifi] -> [fhirServer] : MeassureReport
[someMediator] -> [fhirServer] : Bundle of Measure from History over HTTPs
[someMediator] -> [fhirServer] : Bundle of MeasureReport from History over HTTPs
[someMediator] -> [epicor] : ?
@enduml
```

- Protocol:
  - to TanzaniaHIM: HTTPs
  - to MSD: ?
- Format:
  - to TanzaniaHIM: Measure and MeasureReport as FHIR Bundle from History (todo: links)
- Given: OpenLMIS has created MeasureReports for every
- When: Some Mediator polls/+subscribes to History Bundle of Measure and MeasureReport,
  detects new resources (of either).
- Then: Some Mediator will extraxt the Stock on Hand for each Product x Location, (aggregate it?),
  and send to Epicor ERP.

## Allocated Funds (TZ) (WIP)

```plantuml
@startuml
skinparam componentStyle rectangle

frame MSD {
    component epicor [
        Epicor ERP

        Determines allocated funds(Receipts in Kinds) for each facility updated in Epicor ERP for the government fiscal year.
    ]
}

frame TanzaniaHIM {
    component TanzaniaHIMPassThroughChannel [
        TanzaniaHIM pass through channel

    Receives new facility allocated funds (receipts in-kinds),
    process, validate and send to OpenLMIS via HTTP(s).
    ]
}

frame OpenLMIS {
    component tzOpenlmisBudget [
        TZ OpenLMIS Budget

    Receives facility allocated funds(Receipts in-kinds).
    ]
}

[epicor] -> [TanzaniaHIMPassThroughChannel] : Facility receipts in kinds over HTTP(s)
[TanzaniaHIMPassThroughChannel] -> [tzOpenlmisBudget] : OpenLMIS Facility receipts in kinds via http(s)
@enduml
```

- Protocol: HTTP(s)
  - to TanzaniaHIM: HTTP(s)
  - to OpenLMIS: HTTP(s)
- Format: Allocated funds message (json) ([API example and format link](https://openlmis.atlassian.net/wiki/spaces/TZUP/pages/1984200717/i.+API+Definitions+for+Supplemental+sources+of+funds))
- Given: Allocated funds for a given Facility has been created/updated in MSD Epicor (todo: link)
- When: TanzaniaHIM receives the facility allocated funds, validate the payload and send to OpenLMIS through pass through channel.
- Then: OpenLMIS save facility allocated funds in the database

## Other sources of funds from FFARS (TZ) (WIP)

Background: https://ffars.tamisemi.go.tz/docs/documentation/#1-general-information
 
```plantuml
@startuml
skinparam componentStyle rectangle

frame FFARS {
    component FFARS [
        FFARS Service
        
        Determines other funding sources and actual balance for the facility,
        sends to Muungano Gateway for processing.
    ]
}

frame "Muungano Gateway" {
    component MuunganoGatewayFundSourceMendiator [
        Muungano Gateway fund source Mendiator

        Receives other sources of funds, process and send to TanzaniaHIM over HTTP(s) 
    ]
}

frame TanzaniaHIM {
    component FFARSOpenLMISMediator [
        FFARS OpenLMIS Mediator

        Receives new Other sources of funds, process, validate and send to OpenLMIS via HTTP(s).
    ]
}

frame OpenLMIS {
    component tzOpenlmisBudget [
        TZ OpenLMIS Budget

        Store Other Sources of Funds from OpenHIM in the database.
    ]
}
@enduml
```

- Protocol: HTTP(s)
  - to Muungano Gateway: HTTP(s)
  - to TanzaniaHIM: HTTP(s)
  - to OpenLMIS: HTTP(s)
- Format: Other source of funds message (JSON) ([API definition and format link](https://openlmis.atlassian.net/wiki/spaces/TZUP/pages/1984200717/i.+API+Definitions+for+Supplemental+sources+of+funds))
- Given:Other sources of funds for a particular facility has been approved in FFARS Service,
  processed by Muungano Gateway Mediator, TanzaniaHIM 
- When: Muugano Gatway, TanzaniaHIM receives the facility other sources funds and actual balance, 
  validate the payload and send to OpenLMIS.
- Then: TZ Openlmis Budget service will store facility other sources funds and actual balance in the database

## Master Facility List (TZ) (WIP)

Background: https://openlmis.atlassian.net/l/c/dP0Fp10E

```plantuml
@startuml
skinparam componentStyle rectangle

frame HealthFacilityRegistry {
    component facilityRegistry [
        Facility Registry

        Source of truth for Master Facility List.
    ]
}

frame TanzaniaHIM {
    component HFRTanzaniaHIMMediator [
        HFR TanzaniaHIM Mediator

        Process List of Facilities and share to any external systems i.e OpenLMIS 
    ]
}

frame MSD {
    component epicor [
        Epicor ERP

        Order Fulfillment & Shipping
    ]
}

frame "Some HMIS Org" {
    component dhis2 [
        DHIS2

        Health metrics and indicators
    ]
}

frame OpenLMIS {
    component fhirServer [
        FHIR Server

        Stores derived Location information for use by
        OpenLMIS, and optionally may ammend Location
        resources with additional supply-chain resources.
    ]

    component referenceData [
        Reference Data

        Configures Facilities into Requisition Groups
        with specific Users and Programs.
    ]
}

[someMediator] -> [facilityRegistry] : ? over ?
[someMediator] -> [epicor] : ? over ?
[someMediator] -> [dhis2] : ? over ?
[fhirServer] -> [someMediator] : mCSD Care Services Selective Consumer query over HTTP(s)
[fhirServer] -> [referenceData] : OpenLMIS Facility over HTTP(s)

'layout near one another for clarity
[someMediator] -[hidden]- [fhirServer]
[someMediator] -[hidden]- [epicor]
[someMediator] -[hidden]- [dhis2]
@enduml
```

- Protocol:
  - to TanzaniaHIM: ?
  - to OpenLMIS: HTTP(s)
- Format:
  - to TanzaniaHIM: ?
  - to OpenLMIS: mCSD (todo: link w/ version)
- Given: All facilities/locations we wish to use across systems are represented in the
  Master Facility List in the Facility Registry.
- When: OpenLMIS queries on Facilities/Locations.
- Then: someMediator provides response on Facilities / Locations.

## Stock on Hand and Consumption for LLIN (TIN) (TZ) (WIP)

```plantuml
@startuml
skinparam componentStyle rectangle

frame OpenLMIS {
    component fhirServer [
        FHIR Server

        Store and serve Measure and MeasureReport for
        Metric and Indicators of supplies by
        Product x Location x Time.
    ]
}

frame TanzaniaHIM {
    component someMediator [
        Some Mediator

        ? FHIR to ADX using HFR code?
    ]
}

frame "Some HMIS Org" {
    component dhis2 [
        DHIS2

        Health & Supply chain metrics & indicator
        visualization
    ]
}

[someMediator] -> [fhirServer] : mADX ?
[someMediator] -> [dhis2] : ADX ?

[fhirServer] -[hidden]- [someMediator]
```

- Protocol:
- Format:
- Given:
- When:
- Then:

## CMS Stock on Hand and Consumption for LLIN (ITN) (TZ) (WIP)

TODO:  similar to TZ flow, except no facility registry
TODO:  propose to align on FHIR / mADX on OpenLMIS side - aligns well
  with a future HIM, and in the interm would allow the facility mapping
  to be stored in a common format, though in this case managed through a TZ
  specific UI (ideal case this same UI is re-purposed for future HIM).

- Protocol:
- Format:
- Given:
- When:
- Then:

## Requisition initiation (TZ & ZAN) (WIP)

TODO:  propose that any communication goes through same flow as will exist
in TanzaniaHIM, and that TanzaniaHIM communicates to Muungano Gateway + others

Design page: https://openlmis.atlassian.net/l/c/8iRy13VQ

- Protocol:
- Format:
- Given:
- When:
- Then:
